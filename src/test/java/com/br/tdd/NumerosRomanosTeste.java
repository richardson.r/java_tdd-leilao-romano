package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NumerosRomanosTeste {

    private NumerosRomanos numerosRomanos;

    @Test
    public void testarNumeroNegativo() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            numerosRomanos.conversorDeNumero(-1);
        });
    }

    @Test
    public void testarNumero0() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            numerosRomanos.conversorDeNumero(0);
        });
    }

    @Test
    public void testarNumero1() {
        int numero = 1;
        String resultado = numerosRomanos.conversorDeNumero(numero);

        Assertions.assertEquals("I", resultado);
    }

    @Test
    public void testarNumero7() {
        int numero = 7;
        String resultado = numerosRomanos.conversorDeNumero(numero);

        Assertions.assertEquals("VII", resultado);
    }

    @Test
    public void testarNumero256() {
        int numero = 256;
        String resultado = numerosRomanos.conversorDeNumero(numero);

        Assertions.assertEquals("CCLVI", resultado);
    }
}
