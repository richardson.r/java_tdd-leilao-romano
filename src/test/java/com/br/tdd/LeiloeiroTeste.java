package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeiloeiroTeste {
    private Leiloeiro leiloeiro;
    private Usuario usuario;

    @BeforeEach
    public void setup() {
        this.leiloeiro = new Leiloeiro("Leitão");
        this.usuario = new Usuario(1, "Rich");
    }


    @Test
    public void testarMaiorLance(){
        Lance lanceMenor = new Lance(this.usuario, 9.00);
        leiloeiro.getLeilao().adicionarNovoLance(lanceMenor);

        Lance lanceMaior = new Lance(this.usuario, 10.00);
        leiloeiro.getLeilao().adicionarNovoLance(lanceMaior);

        Lance resultado = leiloeiro.retornarMaiorLance();

        Assertions.assertEquals(lanceMaior, resultado);
    }

    @Test
    public void testarMaiorLanceSemLances(){
        Assertions.assertThrows(RuntimeException.class, ()->{leiloeiro.retornarMaiorLance();});
    }

}
