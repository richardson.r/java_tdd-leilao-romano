package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {
    private Leiloeiro leiloeiro;
    private Usuario usuario;

    @BeforeEach
    public void setup() {
        this.leiloeiro = new Leiloeiro("Leitão");
        this.usuario = new Usuario(1, "Rich");
    }

    @Test
    public void testarAdicionarPrimeiroLance() {
        Lance lance = new Lance(this.usuario, 10.00);
        leiloeiro.getLeilao().adicionarNovoLance(lance);

        Assertions.assertTrue(leiloeiro.getLeilao().getLances().size() == 1);
    }

    @Test
    public void testarAdicionarNovoLance() {
        Lance lance1 = new Lance(this.usuario, 1.00);
        leiloeiro.getLeilao().adicionarNovoLance(lance1);

        Lance lance2 = new Lance(this.usuario, 2.00);
        leiloeiro.getLeilao().adicionarNovoLance(lance1);

        Assertions.assertTrue(leiloeiro.getLeilao().getLances().size() == 2);
    }

    @Test
    public void testarAdicionarLanceMenor(){
        Lance lanceMaior = new Lance(this.usuario, 10.00);
        leiloeiro.getLeilao().adicionarNovoLance(lanceMaior);

        Lance lanceMenor = new Lance(this.usuario, 9.00);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leiloeiro.getLeilao().adicionarNovoLance(lanceMenor);
        });
    }

    @Test
    public void testarMaiorLance(){
        Lance lanceMenor = new Lance(this.usuario, 9.00);
        leiloeiro.getLeilao().adicionarNovoLance(lanceMenor);

        Lance lanceMaior = new Lance(this.usuario, 10.00);
        leiloeiro.getLeilao().adicionarNovoLance(lanceMaior);

        Lance resultado = leiloeiro.retornarMaiorLance();

        Assertions.assertEquals(lanceMaior, resultado);
    }

}
