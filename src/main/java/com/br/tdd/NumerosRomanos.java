package com.br.tdd;

public class NumerosRomanos {

    public static String conversorDeNumero(int numero) {
        if (numero <= 0)
            throw new RuntimeException("Número não pode ser convertido");

        int[] vaNum = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] vaRom = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        String numRomano = "";

        int i = 0;
        while (numero > 0) {
            if (numero >= vaNum[i]) {
                numRomano = numRomano.concat(vaRom[i]);
                numero -= vaNum[i];
            } else
                i++;
        }

        return numRomano;
    }
}
