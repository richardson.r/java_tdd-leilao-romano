package com.br.tdd;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Leiloeiro(String nome) {
        this.nome = nome;
        leilao = new Leilao();
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(){
        int indUltimoLance = leilao.getLances().size()-1;
        return leilao.getLances().get(indUltimoLance);
    }
}
