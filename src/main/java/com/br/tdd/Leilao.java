package com.br.tdd;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao() {
        lances = new ArrayList<Lance>();
    }

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance) {
        Lance lanceOK = validarLance(lance);
        lances.add(lanceOK);

        return lanceOK;
    }

    private Lance validarLance(Lance lance) {
        int ultimo = lances.size() - 1;

        if (lances.size() > 0 && lance.getValorDoLance() < lances.get(ultimo).getValorDoLance())
            throw new RuntimeException("Lance inválido");

        return lance;
    }
}
